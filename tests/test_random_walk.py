"""Test random walk.
"""

import unittest

# import pandas as pd
# from de4l_geodata.geodata.point_t import PointT
# from de4l_geodata.mobility_model import mobility_model
# from de4l_geodata.geodatasets.taxi import TaxiServiceTrajectoryDataset as Td
# from geopy.geocoders import Nominatim
#
# import random_walk.algorithm as rw


class TestRandomWalk(unittest.TestCase):
    """Test random walk.
    """

    # todo: run this test, when Nominatim available from GitLab
    def test_random_walk(self):
        """Test random walk in Porto, Portugal.
        """
        # file_path = "tests/resources/test-taxi-dataset-medium.csv"
        # taxi_dataset = Td.create_from_csv(file_path, max_allowed_speed_kmh=120)
        # nominatim = Nominatim(scheme='http', domain='localhost:8080')
        # model = mobility_model.MobilityModel(path_to_file='tests/resources/taxi_porto_mobility.db',
        #                                      nominatim=nominatim,
        #                                      ors_path='localhost:8008',
        #                                      ors_profile='driving-car')
        # try:
        #     model.calculate_mobility_model(taxi_dataset.data_frame)
        # except:
        #     print()
        #     print(model.nr_routes, 'routes read.')
        # print(model.nr_routes, 'routes read.')
        #
        # # create a random walk
        # start_point = \
        #     PointT([-8.618499, 41.141376], timestamp=pd.Timestamp('2022-01-01 08:00:00'), coordinates_unit='degrees')
        # destination_point = \
        #     PointT([-8.633820845428428, 41.160016947355516], timestamp=pd.Timestamp('2022-01-01 08:12:00'),
        #            coordinates_unit='degrees')
        # allowed_duration_error = 2 * 60
        # max_attempts = 10
        # random_walk_algorithm = rw.RandomWalk(model)
        # found, nr_attempts, walks = \
        #     random_walk_algorithm.calculate_valid_walk(start_point, destination_point, allowed_duration_error,
        #                                                max_attempts)
        # print('No walk found :(') if found else print(walks[-1])

