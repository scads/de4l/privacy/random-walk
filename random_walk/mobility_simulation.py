import random
import pandas as pd
from geopy.geocoders import Nominatim
from shapely.geometry import Polygon

from de4l_geodata.geodata.point_t import PointT
from mobility_model import mobility_model
from random_walk.algorithm import RandomWalk

def group_stay_locations(df: pd.DataFrame):
    """
    Group consecutive rows with the same location and activity.

    Args:
        df (pd.DataFrame): Input dataframe with mobility data.

    Returns:
        pd.DataFrame: Grouped dataframe with aggregated stay locations.
    """
    df = df.sort_values('datetime')
    
    df['group'] = ((df['lat'] != df['lat'].shift()) | 
                   (df['lng'] != df['lng'].shift()) | 
                   (df['activity_ids'] != df['activity_ids'].shift())).cumsum()
    
    grouped = df.groupby('group').agg({
        'lat': 'first',
        'lng': 'first',
        'datetime': 'first',
        'activity_ids': 'first',
        'poi': 'first',
    })

    grouped['leaving_time'] = df.groupby('group')['datetime'].last().values
    grouped.columns = ['lat', 'lng', 'datetime', 'activity_ids', 'poi', 'leaving_time']
    
    df = grouped.reset_index(drop=True)
    return df

def create_bounding_box(data_df: pd.DataFrame, buffer: float = 0.0001) -> Polygon:
    """
    Create a bounding box polygon from dataframe coordinates.

    Args:
        data_df (pd.DataFrame): Dataframe with latitude and longitude.
        buffer (float, optional): Degree buffer around coordinates. Defaults to 0.0001.

    Returns:
        Polygon: Bounding box polygon.
    """
    min_lat = data_df['lat'].min() - buffer
    max_lat = data_df['lat'].max() + buffer
    min_lon = data_df['lng'].min() - buffer
    max_lon = data_df['lng'].max() + buffer
    
    bbox_coordinates = [
        (min_lon, min_lat),
        (min_lon, max_lat),
        (max_lon, max_lat),
        (max_lon, min_lat),
        (min_lon, min_lat)
    ]
    
    bbox = Polygon(bbox_coordinates)
    return bbox


def main():
    """
    Main function to execute mobility simulation.

    Based on a dataset a mobility model is created and random walks between the stay points are calculated and added to the dataset.
    """
    csv_path = 'resources/astra_routes_checkin_agent1.csv'
    database_path = 'resources/nyc.db'

    # Load and preprocess data
    data_df = pd.read_csv(csv_path, sep=';')
    data_df['datetime'] = pd.to_datetime(data_df['datetime'])
    data_df = group_stay_locations(data_df)

    # Initialize geolocation and routing services
    nominatim = Nominatim(user_agent='mobility_simulation', scheme='http', domain='localhost:8080')
    ors_path = "localhost:8080"
    ors_scheme = 'http'

    # ors_client = openrouteservice.Client(key='') #add your openrouteservice user key here
    
    # replace with extracting the actual weekdays and transport modes from the dataset
    weekdays = [6] # 0-6 for monday to sunday
    transport_modes = ['driving-car', 'foot-walking', 'cycling-regular'] 

    # Create mobility model
    model = mobility_model.MobilityModel(
        path_to_file=database_path,
        nominatim=nominatim,
        ors_path=ors_path,
        ors_scheme=ors_scheme,
        transport_modes=transport_modes,
        ors_client=None
    )

    # Generate bounding box 
    bbox = create_bounding_box(data_df)

    # Get mandatory points from the dataset
    mandatory_points = {
        (row['lng'], row['lat']): PointT([row['lng'], row['lat']], timestamp=row['leaving_time'], coordinates_unit='degrees')
        for _, row in data_df.iterrows()
    }
    mandatory_points = list(mandatory_points.values())

    # Calculate mobility model based on the bounding box
    model.calculate_mobility_model_from_bounding_box(bbox, num_pairs=100, mandatory_points=mandatory_points, weekdays=weekdays, num_mandatory_connections=10)
    
    # Random walk configuration
    allowed_duration_error = 10 * 60
    max_attempts = 500
    random_walk_algorithm = RandomWalk(mobility_model=model, transport_modes=transport_modes)

    combined_data = []
    # Generate walks between the points of the dataframe and add all points and walks to combined_data
    for i in range(len(data_df) - 1):
        combined_data.append({
            'coordinates': [data_df['lng'][i], data_df['lat'][i]],
            'start_time': data_df['datetime'][i],
            'end_time': data_df['leaving_time'][i],
            'activity': data_df['activity'][i],
            'poi': data_df['poi'][i],
            'SEX': data_df['SEX'][i],
            'AGE': data_df['AGE'][i],
            'transport_mode': None,
            'type': 'stay_point'
        })
        start_point = PointT([data_df['lng'][i], data_df['lat'][i]], timestamp=data_df['leaving_time'][i], coordinates_unit='degrees')
        destination_point = PointT([data_df['lng'][i + 1], data_df['lat'][i + 1]], timestamp=data_df['datetime'][i + 1], coordinates_unit='degrees')

        if start_point != destination_point:
            # Use random transport mode from the list of transport modes, replace with the actually desired transport mode
            transport_mode = random.choice(transport_modes)
            print(i)
            found, _, walks = random_walk_algorithm.calculate_valid_walk(
                start_point, 
                destination_point, 
                allowed_duration_error, 
                max_attempts, 
                transport_mode=transport_mode
                )

            # If a valid walk was found add it to the combined_data list
            if found:
                combined_data.append({
                    'coordinates': walks[-1],
                    'start_time': start_point.timestamp,
                    'end_time': destination_point.timestamp,
                    'activity': None,
                    'poi': None,
                    'SEX': None,
                    'AGE': None,
                    'transport_mode': transport_mode,
                    'type': 'random_walk'
                })
    
    walk_df = pd.DataFrame(combined_data)
    walk_df.to_csv('../results.csv', index=False)  


if __name__ == "__main__":
    main()