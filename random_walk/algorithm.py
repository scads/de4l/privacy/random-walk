"""A random walk implementation.
"""

import warnings
import random as rd
import datetime

import numpy as np
import pandas as pd
from de4l_geodata.geodata.point import Point, get_distance
from de4l_geodata.geodata.point_t import PointT
from de4l_geodata.geodata.route import Route
from mobility_model import mobility_model as mm


class WalkConstraints:
    """Constraints that should be met by a valid walk.
    """

    def __init__(self, start_point, destination_point, allowed_duration_error, weekday, transport_mode):
        """
        Initialize walk constraints.

        Parameters
        ----------
        start_point : PointT
            The start point of the walk.
        destination_point : PointT
            The destination point to reach with the walk.
        allowed_duration_error : int
            The allowed difference between the actual walk duration and the difference between start and destination
            point timestamps in seconds.
        weekday : range(6)
            The weekday, for which mobility data should be used when a walk is created.
        """
        self.start_point = start_point
        self.destination_point = destination_point
        self.allowed_duration = (destination_point.timestamp - start_point.timestamp).total_seconds()
        self.allowed_duration_error = allowed_duration_error
        self.weekday = weekday
        self.transport_mode = transport_mode


class Walk:
    """A walk consisting of points and their corresponding openstreetmap segment ids.
    """

    def __init__(self, route, segments, distance):
        """
        Initialize this walk.

        Parameters
        ----------
        route : Route
            The route containing this walk's points.
        segments : List
            The openstreetmap segment ids belonging to this walks route points.
        distance : float
            The total distance walked.
        """
        self.route = route
        self.segments = segments
        self.distance = distance
        self.duration = self.get_duration()

    def get_duration(self):
        """
        Returns the duration of this walk in seconds.

        Returns
        -------
        duration : int
            The duration of this walk in seconds.
        """
        duration = 0
        if self.__len__() > 0:
            duration = (self.route[-1].timestamp - self.route[0].timestamp).total_seconds()
        return duration

    def walk_to(self, point, segment, distance):
        """
        Update this walk to walk towards the point, described by the given parameters.

        Parameters
        ----------
        point : PointT
            The point next walked to.
        segment : int
            The openstreetmap segment id of the point walked to.
        distance : float
            The distance that is walked with this move.

        """
        self.segments.append(segment)
        self.route.append(point)
        self.distance += distance
        self.duration = self.get_duration()

    def is_valid(self, constraints: WalkConstraints):
        """
        Check if this walk is valid, given the provided constraints.

        Parameters
        ----------
        constraints : WalkConstraints
            The constraints of this walk.

        Returns
        -------
        valid : bool
            True, if the walk fulfills all constraints, else false.
        """
        valid = False
        if self.__len__() > 0:
            last_point = self.route[-1]
            allowed_min_duration = constraints.allowed_duration - constraints.allowed_duration_error
            allowed_max_duration = constraints.allowed_duration + constraints.allowed_duration_error
            valid = \
                (constraints.destination_point == last_point or get_distance(constraints.destination_point,
                                                                             last_point) < 50) \
                and \
                allowed_min_duration <= self.duration <= allowed_max_duration
        return valid

    def __len__(self):
        """
        The number of points in this walk.

        Returns
        -------
        int
            The number of points in this walk.
        """
        return len(self.route)


class RandomWalk:
    """A random walk implementation.
    """

    def __init__(self, mobility_model: mm.MobilityModel, transport_modes=None, ignore_warnings=False):
        """
        Initializes the random walk algorithm.

        Parameters
        ----------
        mobility_model : mm.MobilityModel
            The mobility model describing locations and transitions between them, that is used to create random walks.
        ignore_warnings : bool
            If true, warnings will be ignored, e.g. when no valid random walk can be calculated.
        """
        self.model = mobility_model
        self.constraints = None
        self.walk = None
        self.ignore_warnings = ignore_warnings
        self.transport_modes = transport_modes if transport_modes else ['driving-car']

    def calculate_walk(self, start_point: PointT, destination_point: PointT, allowed_duration_error, transport_mode):
        """
        Calculates a random walk that starts at the start point and randomly walks along connected street segments until
        it either reaches the destination point or until the duration of the walk exceeds the allowed duration plus the
        given error. The next move is chosen based on the transition probabilities of the underlying mobility model
        as well as the likelihood of walking straight (no steep turns) and walking into the direction of the destination
        point.

        Parameters
        ----------
        start_point : PointT
            The start point of the random walk.
        destination_point : PointT
            The destination point of the random walk.
        allowed_duration_error : int
            The allowed difference between the actual walk duration and the difference between start and destination
            point timestamps in seconds.

        Returns
        -------
        walk_found : bool
            True, if a valid walk was found, else false.
        """
        input_coordinates_unit_is_radians = start_point.get_coordinates_unit() == 'radians'
        start_point = start_point.to_degrees(ignore_warnings=True)
        destination_point = destination_point.to_degrees(ignore_warnings=True)
        start_osm_id = self.model.nominatim.reverse([start_point.y_lat, start_point.x_lon]).raw['osm_id']
        weekday = start_point.timestamp.weekday()

        if transport_mode in self.transport_modes:
            self.constraints = WalkConstraints(start_point, destination_point, allowed_duration_error, weekday, transport_mode)
        else:
            raise Exception(f'This random walk has not been initialized for the requested transport mode {transport_mode}. \
                            The available transport modes are: {self.transport_modes}')
        
        self.walk = Walk(route=Route([start_point]), segments=[start_osm_id], distance=0)
        walk_found = False

        while True:
            possible_next_moves = self.model.find_possible_transition(
                start_osm_id=self.walk.segments[-1],
                forbidden_target_osm_ids=self.walk.segments[:-1],  # allow staying at current segment
                weekday=self.constraints.weekday,
                transport_mode=self.constraints.transport_mode
            )
            next_move = self.get_next_move(possible_next_moves)
            allowed_max_duration = self.constraints.allowed_duration + self.constraints.allowed_duration_error
            if next_move is None or self.walk.duration > allowed_max_duration:
                break  # end this unsuccessful walk
            self.walk.walk_to(
                point=PointT(self.model.find_location(next_move['osm_id_2'])['center'].values[0],
                             timestamp=self.walk.route[-1].timestamp + pd.Timedelta(seconds=next_move['duration_avg']),
                             coordinates_unit='degrees'),
                segment=next_move['osm_id_2'],
                distance=next_move['distance_avg'])
            if self.walk.is_valid(self.constraints):
                if input_coordinates_unit_is_radians:
                    self.walk.route.to_radians_(ignore_warnings=True)
                walk_found = True
                break
        return walk_found

    def calculate_valid_walk(self, start_point: PointT, destination_point: PointT, allowed_duration_error,
                             max_nr_attempts, time_limit=None, transport_mode='driving-car'):
        """
        Calculates random walks until a valid walk is found or max_nr_attempts is reached.

        Parameters
        ----------
        start_point : Point
            The start point.
        destination_point : Point
            The destination point.
        allowed_duration_error : int
            The allowed difference between the actual walk duration and the difference between start and destination
            point timestamps in seconds.
        max_nr_attempts : int
            The maximum number of attempts, that should be undertaken to find a valid walk.
        time_limit : int
            The maximum allowed duration in seconds to find a valid walk. If no valid walk was found in this duration,
            calculation of random walks will be cancelled. If None, there is no time limit.

        Returns
        -------
        valid_walk_found : bool
            True, if a valid walk was found, else false.
        nr_attempts : int
            The number of attempts before a valid walk was found or the maximum number of attempts was reached.
        walk_attempts : List[Route]
            A list containing all random walk attempts.
        """
        walk_attempts = []
        valid_walk_found = False
        start_calculation = datetime.datetime.now()
        for i in range(max_nr_attempts):
            calculation_time = pd.Timedelta(datetime.datetime.now() - start_calculation)
            if time_limit is not None and calculation_time.total_seconds() >= time_limit:
                break
            valid_walk_found = self.calculate_walk(start_point, destination_point, allowed_duration_error, transport_mode)
            walk_attempts.append(self.walk.route)
            if valid_walk_found:
                break
        return valid_walk_found, len(walk_attempts), walk_attempts

    @classmethod
    def get_angle_towards_target(cls, start_point, next_point, destination_point):
        """
        Calculates the angle between the line segments |start_point next_point| and |start_point destination_point|.
        The angle can be between 0 and 180 where a small angles mean, that when heading from start_point to next_point,
        we are also heading towards the destination_point.

        Parameters
        ----------
        start_point : Point
            The start point.
        next_point : Point
            The point headed towards.
        destination_point : Point
            The destination point.

        Returns
        -------
        The angle between the line segments |start_point next_point| and |start_point destination_point|
        """
        distance_a = get_distance(next_point, destination_point)
        distance_b = get_distance(start_point, destination_point)
        distance_c = get_distance(start_point, next_point)

        if start_point == next_point or next_point == destination_point:
            angle = 0
        else:
            angle = np.arccos(
                (np.power(distance_b, 2) + np.power(distance_c, 2) - np.power(distance_a, 2)) /
                (2 * distance_b * distance_c)
            )
            angle = angle * 360 / (2 * np.pi)
        return angle

    def get_next_move(self, possible_next_moves):
        """
        Returns the next move that has been randomly chosen to be done, based on the transition probabilities of all
        possible moves from the current point, the likelihood of walking straight (no steep turns) and the likelihood
        of walking into the direction of the destination point. If no move is possible, None is returned.

        Parameters
        ----------
        possible_next_moves : df.core.frame.DataFrame
            The data frame containing all possible moves from the current point and transition details.

        Returns
        -------
        next_move : df.core.series.Series
            The series containing the single move that has been chosen to do next. If no movements are possible, None
            is returned.
        """
        if self.walk is None:
            raise Exception('No walk initialized.')
        if len(self.walk.route) == 0:
            raise Exception('No start point of walk defined.')

        next_move = None
        data_frame = possible_next_moves.copy()

        if data_frame.empty:
            if not self.ignore_warnings:
                warnings.warn('No move possible.')
        else:
            last_point = None
            if len(self.walk.route) > 1:
                last_point = self.walk.route[-2]
            current_point = self.walk.route[-1]
            destination_point = self.constraints.destination_point

            data_frame['next_point_location'] = data_frame['osm_id_2'].apply(
                lambda osm_id: Point(self.model.find_location(osm_id)['center'].values[0], coordinates_unit='degrees')
            )
            # reward moving into the target direction
            data_frame['angle_towards_target'] = data_frame['next_point_location'].apply(
                lambda next_point: self.get_angle_towards_target(current_point, next_point, destination_point)
            )
            # probability high when angle towards target small
            data_frame['probability_of_moving_towards_target'] = data_frame['angle_towards_target'].apply(
                lambda angle: (180 - angle) / 180
            )

            # encourage moving straight instead of going backwards
            data_frame['angle_with_last_segment'] = data_frame['next_point_location'].apply(
                lambda next_point:
                self.get_angle_towards_target(current_point, next_point, last_point) if last_point is not None else 180
            )
            # probability high when angle big
            data_frame['probability_of_moving_away_from_last_segment'] = data_frame['angle_with_last_segment'].apply(
                lambda angle: angle / 180
            )

            # moving towards target is weighted to be twice as important
            data_frame['transition_probability_towards_target'] = \
                data_frame['transition_probability'] * \
                data_frame['probability_of_moving_away_from_last_segment'] * \
                data_frame['probability_of_moving_towards_target'] * \
                data_frame['probability_of_moving_towards_target']
            angle_sum = data_frame['transition_probability_towards_target'].sum()
            data_frame['transition_probability_towards_target_normalized'] = \
                data_frame['transition_probability_towards_target'] / (angle_sum if angle_sum else 1)

            random = rd.random()
            cumulated_probability = 0
            for _, row in data_frame.iterrows():
                cumulated_probability += row['transition_probability_towards_target_normalized']
                if cumulated_probability > random:
                    next_move = row
                    break
        return next_move
